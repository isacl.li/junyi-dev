# FastDev 計畫

## 目標：
任何一位工程師，5 分鐘內就可以開始開發均一


## How To Start

### 前置安裝

- [Install docker](https://docs.docker.com/get-docker/)
- [Install docker-compose](https://docs.docker.com/compose/install/)
- [Install gcloud](https://cloud.google.com/sdk/docs/install)
- Install git


### Prepare docker image

```sh
gcloud auth login
gcloud auth configure-docker
docker pull gcr.io/junyiacademy/dev/localserver:latest
```

### 準備 codebase

```sh
mkdir junyi
cd junyi
# 於 junyi 資料夾
git clone git@gitlab.com:junyiacademy/junyiacademy.git
git clone git@gitlab.com:junyiacademy/junyicontentservice.git
git clone git@gitlab.com:junyiacademy/junyicontentservice_py3.git
git clone git@gitlab.com:junyiacademy/JunyiTools.git
cp JunyiTools/FastDev/docker-compose.yml .
```

### 準備 datastore 資料

```sh
# 於 junyi 資料夾
./JunyiTools/FastDev/tools/local-init-datastore-sample-data
```


### 跑均一服務

於背景執行服務

```sh
docker-compose up -d
```

檢查服務是否準備好（初次執行會有一些安裝步驟）

```sh
docker-compose logs -f appserver
```

如果服務已準備好，開啟瀏覽器 http://localhost:8080 可看到均一首頁，並且點選「課程」選單應該要看到課程主題。

### 前端 watch

```sh
docker-compose exec appserver /tools/dev-watch
```

## TODOs

- [x] One command to fetch datastore data.
- [ ] Publish dev docker image to DockerHub (no more google account auth)
- [ ] Fix `contentservice_py3` config to support local redis-server
